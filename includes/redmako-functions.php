<?php
function rm_sugarcrm_client() {
	$client = new RedMako_SugarCrm_Client(get_option('sugarcrm_endpoint_url'));
	$client->authenticate(get_option('sugarcrm_username'), get_option('sugarcrm_password'));
	return $client;
}

function rm_email_error($error) {
	$to_email = get_option('sugarcrm_error_log_to_email');

	if ($to_email) {
		wp_mail($to_email, 'SugarCRM push failed (#' . time() . ')', $error);
	}
}

function rm_sugarcrm_push_unit_completion($user_id, $unit_number, $depth = 0, $lastException = null) {
	if ($depth < 5) {
		try
		{
			$client = rm_sugarcrm_client();
			$trainee_id = get_user_meta($user_id, 'sugarcrm_id', true);
			
			if ($trainee_id) {
				$trainee = $client->get_trainee(get_user_meta($user_id, 'sugarcrm_id', true));
				$training_plan = $client->get_training_plan_for_trainee($trainee->id);
				$unit = $client->get_unit_for_training_plan_by_name($training_plan->id, $unit_number);

				$update = array(
					'unit_quiz_assessment' => true
				);

				if ($unit && $unit->id) {
					if ($unit->unit_observation_assessment == 'Approved' && $unit->unit_3rd_party == 'Approved') {
						$update['completed_assessment_date'] = date('Y-m-d');
					}

					$client->update_unit($training_plan->id, $unit->id, $update);
				} else {
					error_log("Unit $unit_number for user $user_id was NOT updated. $unit_number doesn't exist in training plan " . $training_plan->id . ".");
					rm_email_error("Unit $unit_number for user $user_id was NOT updated. $unit_number doesn't exist in training plan " . $training_plan->id . ".");
				}

				return true;
			}
		}
		catch (\Exception $e)
		{
			error_log("Push to SugarCRM failed! Exception: $e");
			error_log("Retrying...");

			rm_sugarcrm_push_unit_completion($user_id, $unit_number, $depth + 1, $e);
		}
	} else {
		error_log("Push to SugarCRM failed 5 times, giving up.");

		$user_data = get_userdata($user_id);
		rm_email_error("Push to SugarCRM failed 5 times.\n" .
		"User: " . $user_data->user_login . "\n" .
		"Unit Number: " . $unit_number . "\n" .
		"Last exception: $lastException\n" .
		"Please check the logs for more information.");
	}
}

add_action('wp_ajax_wp_pro_quiz_completed_quiz', function() {
	register_shutdown_function(function() {
		$post = get_post($_POST['lesson_id']);
		$unit = get_field('unit_number', $post);

		if (get_option('sugarcrm_endpoint_url') && $unit) {
			rm_sugarcrm_push_unit_completion(wp_get_current_user()->ID, $unit);
		}
	});
}, 1);

function rm_exclude_menu_items( $items, $menu, $args ) {
	$user_roles = wp_get_current_user()->roles;
	$hide = false;

	foreach ($user_roles as $role) {
		if ($role == 'subscriber' || $role == 'participant') {
			$hide = true;
		}
	}

	if ($hide) {
		foreach ( $items as $key => $item ) {
			if ( $item->title == 'Tutor Dashboard' ) unset( $items[$key] );
		}
	}

	return $items;
}

add_filter( 'wp_get_nav_menu_items', 'rm_exclude_menu_items', null, 3 );

function rm_previous_post_link($course_id, $lesson_id, $user_id) {
	global $post;
	$permalink = '';
	$link_name = '';

	$hierarchy = apply_filters('immerse_lms_get_course_steps_hierarchy', $course_id, $user_id);

	for ($i = 0; $i < count($hierarchy); $i++) {
		if ($hierarchy[$i]['lesson']->ID == $lesson_id) {
			break;
		}
	}

	if ($post->post_type == 'sfwd-lessons') {
		if ($i > 0) {
			$link_name = sprintf( esc_html_x( 'Previous %s', 'Previous Lesson Label', 'learndash' ), LearnDash_Custom_Label::get_label( 'lesson' ) );
			$permalink = get_permalink($hierarchy[$i - 1]['lesson']->ID);
		}
	} else if ( $post->post_type == 'sfwd-topic' ) {
		$child_id = $post->ID;

		foreach ($hierarchy[$i]['children'] as $c => $child) {
			if ($child->ID == $child_id) {
				break;
			}
	
			$previous_child = $child->ID;
		}
		
		if (isset($previous_child)) {
			$link_name = sprintf( esc_html_x( 'Previous %s', 'Previous Topic Label', 'learndash' ), LearnDash_Custom_Label::get_label( 'topic' ) );
			$permalink = get_permalink($previous_child);
		}
	}

	if ($permalink) {
		if ( is_rtl() ) {
			$link_name_with_arrow = $link_name;
		} else {
			$link_name_with_arrow = '<span class="meta-nav">&larr;</span> ' . $link_name;
		}

		$link = '<a href="'.$permalink.'" class="prev-link" rel="prev">' . $link_name_with_arrow . '</a>';

		 /**
		 * Filter previous post link output
		 * 
		 * @since 2.1.0
		 * 
		 * @param  string  $link 
		 */
		return apply_filters( 'learndash_previous_post_link', $link, $permalink, $link_name, $post );
	}
}

function rm_next_post_link($course_id, $lesson_id, $user_id) {
	global $post;
	$permalink = '';
	$link_name = '';

	$hierarchy = apply_filters('immerse_lms_get_course_steps_hierarchy', $course_id, $user_id);

	for ($i = 0; $i < count($hierarchy); $i++) {
		if ($hierarchy[$i]['lesson']->ID == $lesson_id) {
			break;
		}
	}

	if ($post->post_type == 'sfwd-lessons') {
		if ($i < count($hierarchy) - 1) {
			$link_name = sprintf( esc_html_x( 'Next %s', 'Next Lesson Label', 'learndash' ), LearnDash_Custom_Label::get_label( 'lesson' ) );
			$permalink = get_permalink($hierarchy[$i + 1]['lesson']->ID);
		}
	} else if ( $post->post_type == 'sfwd-topic' ) {
		$child_id = $post->ID;

		foreach ($hierarchy[$i]['children'] as $c => $child) {
			if ($child->ID == $child_id) {
				if (isset($hierarchy[$i]['children'][$c + 1])) {
					$next_child = $hierarchy[$i]['children'][$c + 1]->ID;
				}
				break;
			}
		}
		
		if (isset($next_child)) {
			$link_name = sprintf( esc_html_x( 'Next %s', 'Next Topic Label', 'learndash' ), LearnDash_Custom_Label::get_label( 'topic' ) );
			$permalink = get_permalink($next_child);
		}
	}

	if ($permalink) {
		if ( is_rtl() ) {
			$link_name_with_arrow = $link_name;
		} else {
			$link_name_with_arrow = '<span class="meta-nav">&larr;</span> ' . $link_name;
		}

		$link = '<a href="'.$permalink.'" class="prev-link" rel="prev">' . $link_name_with_arrow . '</a>';

		 /**
		 * Filter previous post link output
		 * 
		 * @since 2.1.0
		 * 
		 * @param  string  $link 
		 */
		return apply_filters( 'learndash_previous_post_link', $link, $permalink, $link_name, $post );
	}
}

function rm_previous_lesson_completed($course_id, $lesson_id, $user_id) {
	$previous_lesson_completed = true;

	$hierarchy = apply_filters('immerse_lms_get_course_steps_hierarchy', $course_id, $user_id);

	for ($i = 0; $i < count($hierarchy); $i++) {
		if ($hierarchy[$i]['lesson']->ID == $lesson_id) {
			break;
		}
	}

	if ($i > 0) {
		// Check if the previous lesson was completed.
		$i--;

		while (isset($hierarchy[$i]) && in_array(0, apply_filters('immerse_lms_learndash_lesson_progress', $course_id, $hierarchy[$i], $user_id))) {
			$previous_item = $hierarchy[$i]['lesson'];
			$previous_lesson_completed = false;
			$i--;
		}
	}

	return $previous_lesson_completed;
}

function rm_quiz_lesson($course_id, $user_id, $quiz_id) {
	$hierarchy = apply_filters('immerse_lms_get_course_steps_hierarchy', $course_id, $user_id);

	foreach ($hierarchy as $step) {
		$quiz = array_filter($step['children'], function($c) use ($quiz_id) { return $c->ID == $quiz_id; });

		if ($quiz) {
			return $step['lesson'];
		}
	}

	return null;
}

function rm_previous_child_completed($course_id, $lesson_id, $child_id, $user_id) {
	$hierarchy = apply_filters('immerse_lms_get_course_steps_hierarchy', $course_id, $user_id);

	for ($i = 0; $i < count($hierarchy); $i++) {
		if ($hierarchy[$i]['lesson']->ID == $lesson_id) {
			break;
		}
	}

	$lesson_progress = apply_filters('immerse_lms_learndash_lesson_progress', $course_id, $hierarchy[$i], $user_id);
	$previous_child = null;

	foreach ($hierarchy[$i]['children'] as $c => $child) {
		if ($child->ID == $child_id) {
			break;
		}

		$previous_child = $child->ID;
	}

	return !$previous_child || $lesson_progress[$previous_child];
}

function filter_search_item_types($option) {
	$index = array_search('members', $option);

	if ($index !== FALSE) {
		array_splice($option, $index, 1);
		array_splice($option, 0, 0, 'members');
	}

	return $option;
}
add_filter('buddyboss_global_search_option_items-to-search', 'filter_search_item_types');

function rm_footer_tracking_code() {
	?>
	<script type="text/javascript">var rumMOKey='b71f30e93a8d5b2f33f3dd60f1f8240c';(function(){if(window.performance && window.performance.timing && window.performance.navigation) {var site24x7_rum_beacon=document.createElement('script');site24x7_rum_beacon.async=true;site24x7_rum_beacon.setAttribute('src','//static.site24x7rum.com/beacon/site24x7rum-min.js?appKey='+rumMOKey);document.getElementsByTagName('head')[0].appendChild(site24x7_rum_beacon);}})(window)</script>
	<?php
}
add_action('wp_footer', 'rm_footer_tracking_code');