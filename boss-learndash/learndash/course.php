<?php
/**
 * Displays a course
 *
 * Available Variables:
 * $course_id 		: (int) ID of the course
 * $course 		: (object) Post object of the course
 * $course_settings : (array) Settings specific to current course
 *
 * $courses_options : Options/Settings as configured on Course Options page
 * $lessons_options : Options/Settings as configured on Lessons Options page
 * $quizzes_options : Options/Settings as configured on Quiz Options page
 *
 * $user_id 		: Current User ID
 * $logged_in 		: User is logged in
 * $current_user 	: (object) Currently logged in user object
 *
 * $course_status 	: Course Status
 * $has_access 	: User has access to course or is enrolled.
 * $materials 		: Course Materials
 * $has_course_content		: Course has course content
 * $lessons 		: Lessons Array
 * $quizzes 		: Quizzes Array
 * $lesson_progression_enabled 	: (true/false)
 * $has_topics		: (true/false)
 * $lesson_topics	: (array) lessons topics
 *
 * @since 2.1.0
 *
 * @package LearnDash\Course
 */
?>

<script src="<?php echo bloginfo('stylesheet_directory'); ?>/js/circle-progress.min.js"></script>

<!-- Container added by boss -->
<div class="lms-post-content">
	<?php if ( $logged_in ) : ?>
		<?php
			/**
			 * Filter to add custom content after the Course Status section of the Course template output.
			 * @since 2.3
			 * See https://bitbucket.org/snippets/learndash/7oe9K for example use of this filter.
			 */
			echo apply_filters('ld_after_course_status_template_container', '', learndash_course_status_idx( $course_status ), $course_id, $user_id );
		?>

		<?php  if ( ! empty( $course_certficate_link ) ) : ?>
			<div id="learndash_course_certificate">
				<a href='<?php echo esc_attr( $course_certficate_link ); ?>' class="btn-blue" target="_blank"><?php echo apply_filters('ld_certificate_link_label', __( 'PRINT YOUR CERTIFICATE', 'boss-learndash' ), $user_id, $post->ID ); ?></a>
			</div>
			<br />
		<?php endif; ?>
	<?php endif; ?>

	<?php echo $content; ?>
</div>

<?php
/**
 * Payment Buttons
 */
if ( !$has_access ) :
	echo boss_edu_payment_buttons( $post );
endif;

/**
 * Course Content
 */
if ( $has_course_content ) {

	$show_course_content = true;

	if ( !$has_access ) :
		if ( $course_meta[ 'sfwd-courses_course_disable_content_table' ] == 'on' ) :
			$show_course_content = false;
		endif;
	endif;

	if ( $show_course_content ) :

		/**
		 * Course Materials
		 */
        if ( ( isset( $materials ) ) && ( !empty( $materials ) ) ) :
			?>
			<div id="learndash_course_materials">
				<h4><?php printf( _x( '%s Materials', 'Course Materials Label', 'boss-learndash' ), LearnDash_Custom_Label::get_label( 'course' ) ); ?></h4>
				<div class="materials-content"><?php echo $materials; ?></div>
			</div><?php
		endif;
		?>

		<div id='learndash_course_content'>

			<?php
			/* Show Lesson List */
			$lessons = apply_filters('immerse_lms_get_course_steps_hierarchy', null, $user_id);
			
			if ( !empty( $lessons ) ) {
				?>

				<div id="learndash_lessons" class="learndash_lessons">

					<div id="lesson_heading">
						<span>Units</span>

						<?php if ( $has_topics ) : ?>
							<div class="expand_collapse">
								<a href="#" onClick='jQuery( "#learndash_post_<?php echo $course_id; ?> .learndash_topic_dots" ).slideDown();
				                        return false;'><?php _e( 'Expand All', 'boss-learndash' ); ?></a> |
								<a href="#" onClick='jQuery( "#learndash_post_<?php echo esc_attr( $course_id ); ?> .learndash_topic_dots" ).slideUp();
				                        return false;'><?php _e( 'Collapse All', 'boss-learndash' ); ?></a>
							</div>
						<?php endif; ?>
					</div>

					<div id="lessons_list" class="lessons_list">
						<?php foreach ( $lessons as $lesson ) { ?>
							<?php
							/* Lesson Topics */
							$topics	 = $lesson['children'];
							$meta	 = get_post_meta( $lesson[ 'lesson' ]->ID, '_' . $lesson[ 'lesson' ]->post_type );
							
							/* Lesson Progress */
							$progress = apply_filters('immerse_lms_learndash_lesson_progress', $course_id, $lesson);
							$completed_count = count(array_filter($progress, function ($item) { return $item; }));
							$incomplete_count = count(array_filter($progress, function ($item) { return !$item; }));

							/* Lesson meta info */
							$lesson_meta_data = array();

							if ( is_array( $topics ) && 0 < count( $topics ) ) {
								$lesson_meta_data[] = sprintf( _n( '%s topic', '%s topics', count( $topics ), 'boss-learndash' ), count( $topics ) );
							}

							if ( !empty( $meta[ 0 ][ $lesson[ 'lesson' ]->post_type . '_forced_lesson_time' ] ) ) {
								$lesson_meta_data[] = $meta[ 0 ][ $lesson[ 'lesson' ]->post_type . '_forced_lesson_time' ];
							}
							?>

							<div class="lesson post-<?php echo $lesson[ 'lesson' ]->ID; ?> <?php echo isset($lesson[ "sample" ]) ? $lesson[ "sample" ] : ''; ?> <?php echo (empty( $topics )) ? 'no-topics' : 'has-topics' ?>">
								<h4>
									<?php

									$course_status = '';
									$course_progress_point = '';

									if( $completed_count > 0 ):

										if( $incomplete_count == 0 ): ?>

											<div class="course-completed-icon"></div>

										<?php else:

											$course_progress_point = $completed_count / count($progress); ?>

											<div class="course-circle-progress-bar" data-value="<?php echo $course_progress_point; ?>"></div>

										<?php endif;

									else: ?>

										<div class="course-not-completed-icon"></div>

									<?php endif; ?>

									<a class="<?php echo $completed_count > 0 ? ($incomplete_count == 0 ? 'completed' : 'in-progress') : 'notcompleted'; ?>" href="<?php echo get_the_permalink($lesson['lesson']) ?>"><?php echo $lesson[ 'lesson' ]->post_title; ?></a>

									<p class="lesson-meta"> 
										<span class="lesson-meta-data"><?php echo implode( ', ', $lesson_meta_data ); ?></span>
										<span class="drop-list fa fa-chevron-down"></span>
									</p>
								</h4>

								<?php
								/* Not available message for drip feeding lessons */
								if ( !empty( $lesson[ "lesson_access_from" ] ) ) {
                                    SFWD_LMS::get_template(
                                        'learndash_course_lesson_not_available',
                                        array(
                                            'user_id'					=>	$user_id,
                                            'course_id'					=>	learndash_get_course_id( $lesson['post']->ID ),
                                            'lesson_id'					=>	$lesson['post']->ID,
                                            'lesson_access_from_int'	=>	$lesson['lesson_access_from'],
                                            'lesson_access_from_date'	=>	learndash_adjust_date_time_display( $lesson['lesson_access_from'] ),
                                            'context'					=>	'course'
                                        ), true
                                    );
								}

								if ( !empty( $topics ) ) {
									?>
									<div id="learndash_topic_dots-<?php echo esc_attr( $lesson[ 'lesson' ]->ID ); ?>" class="learndash_topic_dots type-list">
										<ul>
											<?php
											$odd_class = '';

											foreach ( $topics as $key => $topic ) {
												$odd_class		 = empty( $odd_class ) ? 'nth-of-type-odd' : '';
												$completed_class = !$progress[$topic->ID] ? 'topic-notcompleted' : 'topic-completed';
												?>
												<li class="<?php echo $odd_class; ?>">
													<span class="topic_item">
														<a class="<?php echo $completed_class; ?>" href="<?php echo get_permalink( $topic->ID ); ?>" title="<?php echo $topic->post_title; ?>">
															<?php echo $topic->post_title; ?>
														</a>
													</span>
												</li><?php
											}
											?>
										</ul>
									</div><?php
								}
								?>

							</div><?php
						}
						?>
					</div>

				</div><?php
			}

			/* Show Quiz List */
			if ( !empty( $quizzes ) ) {
				?>
				<div id='learndash_quizzes'>
					<div id="quiz_heading"><span><?php echo LearnDash_Custom_Label::get_label( 'quizzes' ) ?></span></div>
					<div id="quiz_list">
						<?php foreach ( $quizzes as $quiz ) { ?>
							<?php // print_r($quiz); ?>
							<div id="post-<?php echo isset($quiz['lesson']) ? $quiz[ 'lesson' ]->ID : null; ?>" class="<?php echo isset($quiz[ "sample" ]) ? $quiz[ "sample" ] : null; ?>">
								<h4>
									<a class="<?php echo $quiz[ "status" ]; ?>" href="<?php echo $quiz[ "permalink" ] ?>"><?php echo $quiz[ 'post' ]->post_title; ?></a>
								</h4>
							</div>
						<?php } ?>
					</div>
				</div>
				<?php
			}
			?>

		</div><?php
	endif;
}

?>

<style>
#learndash_course_content .learndash_lessons .lessons_list .lesson > h4 .course-circle-progress-bar canvas {
	transform: rotate(90deg);
}

#learndash_course_content .learndash_lessons .lessons_list .lesson > h4 .in-progress:before,
#learndash_course_content .learndash_lessons .lessons_list .lesson > h4 .completed:before,
#learndash_course_content .learndash_lessons .lessons_list .lesson > h4 .notcompleted:before {
	content: none !important;
}

#learndash_course_content .learndash_lessons .lessons_list .lesson > h4 .course-completed-icon:before {
    font-family: 'Font Awesome 5 Free';
    display: inline-block;
    font-size: 150%;
    font-weight: 900;
    line-height: 1em;
    width: 1em;
    top: 0;
    color: rgba(0,0,0,.1);
    content: "\f00c";
    color: #ffffff;
    background: #4aaf83;
    display: inline-block;
    font-size: 31px;
    vertical-align: middle;
    line-height: 1;
    text-align: center;
    width: 23px;
    border: 3px solid #4aaf83;
    border-radius: 30rem;
    font-size: 11px;
    height: 15px;
    padding-top: 7px;
}

#learndash_course_content .learndash_lessons .lessons_list .lesson > h4 .course-not-completed-icon:before {
	font-family: 'Font Awesome 5 Free';
    display: inline-block;
    font-size: 150%;
    font-weight: 900;
    line-height: 1em;
    width: 1em;
    top: 0;
    color: rgba(0,0,0,.1);
    content: '\f111';
    color: #d6d8da;
    font-weight: 400;
    display: inline-block;
    font-size: 31px;
    vertical-align: middle;
    line-height: 1;
    text-align: center;
    width: 28px;
}

#learndash_course_content .learndash_lessons .lessons_list .lesson > h4 {
	justify-content: unset;
	align-items: center;
	padding: 20px 30px;
}

#learndash_course_content .learndash_lessons .lessons_list .lesson > h4 .course-circle-progress-bar {
    padding-top: 7px;
}

#learndash_course_content .learndash_lessons .lessons_list .lesson > h4 > a {
    margin-left: 30px;
    padding: 0;
}

#learndash_course_content .learndash_lessons .lessons_list .lesson > h4 .lesson-meta {
    margin-left: auto !important;
}

#learndash_course_content .learndash_lessons .lessons_list .lesson > h4 .course-not-completed-icon,
#learndash_course_content .learndash_lessons .lessons_list .lesson > h4 .course-completed-icon {
    margin: 7px 0;
}

#learndash_course_content .learndash_lessons .lessons_list .lesson .drop-list {
    transform: rotate(90deg);
}

#learndash_course_content .learndash_lessons .lessons_list .lesson.expanded .drop-list {
    transform: rotate(0deg);
    transition: .3s all ease;
}

#learndash_course_content .learndash_lessons .lessons_list .lesson .learndash_topic_dots ul li {
    padding: 15px 30px 15px 91px !important;
}

#learndash_course_content .learndash_lessons .lessons_list .lesson .learndash_topic_dots ul li .topic_item > a.topic-notcompleted:before {
    margin-left: 31px !important;
}

#learndash_course_content .learndash_lessons .lessons_list .lesson .learndash_topic_dots ul li .topic_item > a.topic-completed:before {
    font-family: 'Font Awesome 5 Free';
    font-weight: 900;
    top: 0;
    content: "\f00c" !important;
    color: #ffffff !important;
    background: #4aaf83;
    display: inline-block;
    vertical-align: middle;
    line-height: 1;
    text-align: center;
    width: 14px !important;
    border: 3px solid #4aaf83;
    border-radius: 30rem;
    font-size: 9px !important;
    height: 11px !important;
    padding-top: 3px;
    margin-left: 36px !important;
}
</style>

<script>
(function($) {

	// circle progress bar init
    $('.course-circle-progress-bar').circleProgress({
	    size: 30,
	    thickness: 3,
	    fill: {
	    	color: "#4aaf83"
	    }
	  });

})( jQuery );
</script>