<?php
/**
 * @package Boss Child Theme
 * The parent theme functions are located at /boss/buddyboss-inc/theme-functions.php
 * Add your own functions in this file.
 */

/**
 * Sets up theme defaults
 *
 * @since Boss Child Theme 1.0.0
 */
function boss_child_theme_setup()
{
  /**
   * Makes child theme available for translation.
   * Translations can be added into the /languages/ directory.
   * Read more at: http://www.buddyboss.com/tutorials/language-translations/
   */

  // Translate text from the PARENT theme.
  load_theme_textdomain( 'boss', get_stylesheet_directory() . '/languages' );

  // Translate text from the CHILD theme only.
  // Change 'boss' instances in all child theme files to 'boss_child_theme'.
  // load_theme_textdomain( 'boss_child_theme', get_stylesheet_directory() . '/languages' );

}
add_action( 'after_setup_theme', 'boss_child_theme_setup' );

/**
 * Enqueues scripts and styles for child theme front-end.
 *
 * @since Boss Child Theme  1.0.0
 */
function boss_child_theme_scripts_styles()
{
  /**
   * Scripts and Styles loaded by the parent theme can be unloaded if needed
   * using wp_deregister_script or wp_deregister_style.
   *
   * See the WordPress Codex for more information about those functions:
   * http://codex.wordpress.org/Function_Reference/wp_deregister_script
   * http://codex.wordpress.org/Function_Reference/wp_deregister_style
   **/

  /*
   * Styles
   */
  $the_theme = wp_get_theme();
  wp_enqueue_style( 'boss-child-custom', get_stylesheet_directory_uri().'/css/custom.css', array(), $the_theme->get( 'Version' ), 'all' );
}
add_action( 'wp_enqueue_scripts', 'boss_child_theme_scripts_styles', 9999 );


/****************************** CUSTOM FUNCTIONS ******************************/

// Add your own custom functions here
/*
add_action( 'bp_setup_nav', 'redmako_bp_setup_nav' );

function redmako_bp_setup_nav() {
  global $bp;

  bp_core_new_nav_item(array( 
    'name' => __( 'Quiz History', 'redmako' ), 
    'slug' => 'quiz-history',
    'position' => 100,
    'screen_function' => 'redmako_bp_quiz_history',
    'show_for_displayed_user' => true,
    'item_css_id' => 'redmako_bp_quiz_history'
  ));
}
*/
// Autoloader
foreach (glob(__DIR__ . "/includes/*.php") as $filename)
{
    require_once($filename);
}

function rm_bp_add_profile_extra_custom_links() {
  if (current_user_can('tutor_admin') || current_user_can('administrator')) {
    $username = apply_filters('immerse_lms_displayed_user_name', null);

    ?>

      <li><a href="<?= esc_url(get_site_url(null, 'quiz-history/?user=' . $username)); ?>">Quiz History</a></li>
      <li><a href="<?= esc_url(get_site_url(null, 'course-notes/?user=' . $username)); ?>">Course Notes</a></li>
      <li><a href="<?= esc_url(get_site_url(null, 'completed-unit-report/?user=' . $username)); ?>">Completed Unit Report</a></li>
      <li><a href="<?= esc_url(get_site_url(null, 'course-flow/?user=' . $username)); ?>">Course Flow</a></li>
      
    <?php
  }

  // moved other links from sidebar menu to student profile menu
  if( bp_is_my_profile() ) {

    $username = apply_filters('immerse_lms_displayed_user_name', null);

    ?>

    <li><a href="<?= esc_url(get_site_url(null, 'members/' . $username . '/achievements/')); ?>">My Achievements</a></li>
    <li><a href="<?= esc_url(get_site_url(null, 'members/' . $username . '/settings/')); ?>">My Settings</a></li>
    <li><a href="<?= esc_url(get_site_url(null, 'my-account/')); ?>">Billing & Orders</a></li>

    <?php
  }

}
add_action( 'bp_member_options_nav', 'rm_bp_add_profile_extra_custom_links' );

function redmako_enqueue_scripts() {
  $the_theme = wp_get_theme();
  wp_enqueue_script( 'redmako_functions', get_stylesheet_directory_uri() . '/js/functions.js', array('jquery'), $the_theme->get( 'Version' ), 'all' );
}
add_action('wp_enqueue_scripts', 'redmako_enqueue_scripts', 9999);

function rm_add_quiz_attempts_content($content) {
  global $post;
  
  $attempts = 0;
  $last_test_incorrect = false;
  
  if ($post->post_type == 'sfwd-quiz') {
    $course = apply_filters('immerse_lms_get_course_for_quiz', $post->ID);
    $max_quiz_attempts = apply_filters('immerse_lms_get_max_quiz_attempts', $course->ID, $post->ID);
    
    if ($max_quiz_attempts) {
      $quiz_id = get_post_meta($post->ID, 'quiz_pro_id')[0];

      $ref_mapper = new WpProQuiz_Model_StatisticRefMapper();
      $stat_mapper = new WpProQuiz_Model_StatisticMapper();
      $attempts = $ref_mapper->fetchAll($quiz_id, wp_get_current_user()->ID);

      if (count($attempts)) {
        $last_attempt = $attempts[count($attempts) - 1];
        $stats = $stat_mapper->fetchAllByRef($last_attempt->getStatisticRefId());

        foreach ($stats as $stat) {
          if ($stat->getPoints() == 0) {
            $last_test_incorrect = true;
          }
        }
      }


      return '<script>var QUIZ_ATTEMPTS = ' . json_encode(count($attempts)) . '; var LAST_TEST_INCORRECT = ' . json_encode($last_test_incorrect) . '; var MAX_QUIZ_ATTEMPTS = ' . json_encode($max_quiz_attempts) .  ';</script>' . $content;
    }
  }

  return $content;
}
add_filter('the_content', 'rm_add_quiz_attempts_content');

function rm_add_quiz_attempts_classes($classes) {
  global $post;
  $attempts = 0;

  if ($post->post_type == 'sfwd-quiz') {
    $course = apply_filters('immerse_lms_get_course_for_quiz', $post->ID);
    $max_quiz_attempts = apply_filters('immerse_lms_get_max_quiz_attempts', $course->ID, $post->ID);
    
    if ($max_quiz_attempts) {
      $quiz_id = get_post_meta($post->ID, 'quiz_pro_id')[0];

      $ref_mapper = new WpProQuiz_Model_StatisticRefMapper();
      $attempts = count($ref_mapper->fetchAll($quiz_id, wp_get_current_user()->ID));
      $classes[] = $attempts >= $max_quiz_attempts ? 'cant-test' : 'can-test';
    }
  }
  
  return $classes;
}
add_filter('body_class', 'rm_add_quiz_attempts_classes');

function rm_ldnt_template_archive_note_listing() {
  return __DIR__ . '/learndash-notes/archive-note-listing.php';
}
add_filter('ldnt_template_archive-note-listing', 'rm_ldnt_template_archive_note_listing');

function rm_widgets_init() {
  unregister_widget('Boss_LearnDash_Course_Progress_Widget');
  register_widget('RedMako_LearnDash_Course_Progress_Widget');
}

add_action('widgets_init', 'rm_widgets_init');

/*
* Cleans cart for logged out users
*/
function immerse_lms_clean_cart() {
	// if action is logout (wp_logout didn't work on this case.)
	if (isset($_GET) && isset($_GET['action']) && $_GET['action'] == 'logout' && function_exists('WC')) {
		// 	WC()->cart->empty_cart();	
		global $woocommerce;
		// get user details
		global $current_user;
		get_currentuserinfo();
		$user_id = $current_user->ID;
		$cart_contents = $woocommerce->cart->get_cart();
		$meta_value = $cart_contents;
		update_user_meta( $user_id, "_immerse_lms_saved_cart", $meta_value);
		WC()->cart->empty_cart();
	}
}
add_action('wp_loaded', 'immerse_lms_clean_cart');

function immerse_lms_restore_cart( $user_login, $user ) {
	global $woocommerce;
	$user_id = $user->ID;
	
	$cart_contents = get_user_meta($user_id, '_immerse_lms_saved_cart', true);

  // add cart contents
  if ($cart_contents) {
    foreach ( $cart_contents as $cart_item_key => $values )
    {
      $id =$values['product_id'];
      $quant=$values['quantity'];
      $woocommerce->cart->add_to_cart( $id, $quant);
    }
  }
}
add_action('wp_login', 'immerse_lms_restore_cart', 10, 2);

/**
 * Add default admin login styles.
 */
function enqueue_login_styles() {
    wp_enqueue_style('admin_styles' , get_stylesheet_directory_uri().'/css/admin-login.css');
}

add_action('login_enqueue_scripts', 'enqueue_login_styles');

/**
 * Add privacy policy and procedure link on top menu on logged-in users
 */
function add_privacy_policy_procedure_link($items, $args) {

    if (is_user_logged_in()) {

        $privacy_policy_procedure_link = get_permalink( get_page_by_path( 'privacy-policy-procedure' ) );

        if( $args->menu->slug == 'titlebar')  {
            $items .= '<li class="menu-item menu-item-custom"><a href="' . $privacy_policy_procedure_link . '" class="fa-file">Privacy Policy and Procedure</a></li>';
        }

    }

    return $items;
}

add_filter('wp_nav_menu_items', 'add_privacy_policy_procedure_link', 10, 2);

/**
 * Add homepage partner schools shortcode
 */
function homepage_partner_schools_func( $atts ) {

  $location = 'template/homepage-partner-schools.php';

  ob_start();

  require_once plugin_dir_path( __FILE__ ) . $location;

  return ob_get_clean();
 
}
add_shortcode( 'partner-schools', 'homepage_partner_schools_func' );

function wpdevlms_hide_correct() {
  global $post;

  if ( is_singular( 'sfwd-quiz' ) || ( !empty( $post ) && has_shortcode( $post->post_content, 'LDAdvQuiz' ) ) ) {
    ?>
    <script type="text/javascript">
      jQuery(document).ready(function(){

        $(document).on("click", 'input[name="reShowQuestion"]', function() {

          if( ! $(this).attr('data-loaded') ) {

            $('.wpProQuiz_quiz').hide();

            $('input[name="reShowQuestion"]').parent().addClass('loader-content');

            $.ajax({
              method: "POST",
                url: ajaxurl,
                data: {
                action: "immerse_lms_ajax_get_student_answer",
                quiz_id: <?php echo $post->ID; ?>,
                user_id: <?php echo get_current_user_id(); ?> 
              },
              success: function(data) {
                if( data.success) {
                  var result = data.data;
                  if( Object.keys(result).length ) {
                    for (var key in result) {
                      var resultData = result[key];
                      var parentElement = $('.wpProQuiz_list').find('.wpProQuiz_questionList[data-question_id="' + key + '"] .wpProQuiz_questionListItem');
                      parentElement.each(function(elemKey, elemVal) {
                        $(this).find('.wpProQuiz_sortable').html(resultData[elemKey].text);
                      });
                    }
                  }
                  $('.wpProQuiz_quiz').show();
                  $('input[name="reShowQuestion"]').attr('data-loaded', true);
                  $('input[name="reShowQuestion"]').parent().removeClass('loader-content');
                }
              }
            });

            jQuery(".wpProQuiz_list .wpProQuiz_listItem").each(function(key, value){
              if('single' == jQuery(this).attr('data-type')) {
                if('none' != jQuery(this).find('.wpProQuiz_incorrect').css('display')) {
                  jQuery(this).find('.wpProQuiz_questionList .wpProQuiz_questionListItem').removeClass('wpProQuiz_answerCorrect');
                }
              } else if('multiple' == jQuery(this).attr('data-type')) {
                jQuery(this).find('.wpProQuiz_questionList li').each(function(li_key, li_elem){
                  if(jQuery(this).hasClass('wpProQuiz_answerCorrect') && !(jQuery(this).find('.wpProQuiz_questionInput').is(':checked'))) {
                    jQuery(this).removeClass('wpProQuiz_answerCorrect');
                  }
                });
              }
            });

          }

        });
      });
    </script>
    <?php
  }
}
add_action( 'wp_footer', 'wpdevlms_hide_correct' );

add_filter( 'lostpassword_url',  'rm_lostpassword_url', 10, 0 );
function rm_lostpassword_url() {
  return site_url('/wp-login.php?action=lostpassword');
}