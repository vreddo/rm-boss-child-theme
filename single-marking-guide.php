<?php
include __DIR__ . '/template/page-start.php';

if (!is_user_logged_in()) {
    auth_redirect();
}

if (!current_user_can('tutor_admin') && !current_user_can('administrator') && !current_user_can('trainer') && !current_user_can('marker')) {
    wp_die('You are not authorized to view this page.');
}

global $post;
$selected_course = get_post(get_post_meta($post->ID, '_course', true));
$selected_quiz = get_post(get_post_meta($post->ID, '_quiz', true));
?>
Course: <b><?= $selected_course->post_title ?></b><br />
Quiz: <b><?= $selected_quiz->post_title ?></b><br />
<br />
<h3 style="margin-bottom: 0px;">Overall Guide</h3>
<?= $post->post_content ?>
<?php
$quiz_pro_id = apply_filters('immerse_lms_get_quiz_pro_id', $selected_quiz->ID);

$questionMapper = new WpProQuiz_Model_QuestionMapper();
$questions = $questionMapper->fetchAll($quiz_pro_id);
?>
<?php foreach ($questions as $question): ?>
<div style="margin-top: 16px;">
	<h3 style="margin-bottom: 0px;"><?= __('Guide for') ?> <?= $question->getTitle() ?></h3>
	<div style="margin: 12px; border: 1px solid rgb(100, 100, 100); padding: 8px;">
        <?= $question->getQuestion() ?>
    </div>
	<?php
	$meta_key = '_guide_question_' . $question->getQuestionId();
	echo get_post_meta($post->ID, $meta_key, true);
	?>
</div>
<?php endforeach ?>
<?php
include __DIR__ . '/template/page-end.php';
?>