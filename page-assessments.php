<?php
include __DIR__ . '/template/page-start.php';

if (!is_user_logged_in()) {
    auth_redirect();
}

$user = (isset($_GET['user']) && (current_user_can('tutor_admin') || current_user_can('administrator'))) ? get_user_by('login', $_GET['user']) : wp_get_current_user();

$statRefMapper = new ImmerseLms_Model_StatisticRefMapper();
$statMapper = new WpProQuiz_Model_StatisticMapper();
?>
<b>Status Filter:</b>
<a href="javascript:$('tr.assessment').show();" class="filter">All</a> |
<a href="javascript:$('tr.assessment').hide(); $('tr.assessment[data-status=Read]').show();" class="filter">Read</a> |
<a href="javascript:$('tr.assessment').hide(); $('tr.assessment[data-status=Graded]').show();" class="filter">Graded</a><br />
<Br />
<table>
    <thead>
        <tr>
            <th>Student</th>
            <th>Status</th>
            <th>Grade</th>
            <th>Assigned Course</th>
            <th>Assigned Lesson</th>
            <th>Assessment Title</th>
            <th>Date and Time Uploaded</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $assessments = $statRefMapper->fetchAssessments();

        foreach ($assessments as $assessment)
        {
            $quizPost = get_post(learndash_get_quiz_id_by_pro_quiz_id($assessment->getQuizId()));
            $course = apply_filters('immerse_lms_get_course_for_quiz', $quizPost->ID);
            $lesson = apply_filters('immerse_lms_get_lesson_for_quiz', $quizPost->ID);
            $stats = $statMapper->fetchAllByRef($assessment->getStatisticRefId());
            $assessment_status = 'Graded';

            foreach ($stats as $stat) {
                $answerData = json_decode($stat->getAnswerData(), true);

                if (isset($answerData['graded_id'])) {
                    $status = get_post_status($answerData['graded_id']);

                    if ($status == 'not_graded') {
                        $assessment_status = 'Read';
                    }
                }
            }

            $assessment_grade = 'None';

            if ($assessment_status == 'Graded') {
                if ($assessment->getIncorrectCount() > 0) {
                    $assessment_grade = 'Not Yet Compenent';
                } else {
                    $assessment_grade = 'Compenent';
                }
            }

            ?>
            <tr class="assessment" data-grade="<?= $assessment_grade ?>" data-status="<?= $assessment_status ?>">
                <td><?= $assessment->getUserName() ?></td>
                <td><?= $assessment_status ?></td>
                <td><?= $assessment_grade ?></td>
                <td><?= $course->post_title ?></td>
                <td><?= $lesson['lesson']->post_title ?></td>
                <td><?= $quizPost->post_title ?></td>
                <td><?= date(get_option('date_format'), $assessment->getCreateTime()) ?> <?= date(get_option('time_format'), $assessment->getCreateTime()) ?></td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
<?php
include __DIR__ . '/template/page-end.php';
?>