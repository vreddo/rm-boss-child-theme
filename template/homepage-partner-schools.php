<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('stylesheet_directory'); ?>/css/slick.css">
<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('stylesheet_directory'); ?>/css/slick-theme.css">

<style>
.slick-track {
	display: flex;
	align-items: center;
}

.slick-arrow {
	box-shadow: none !important;
}

.slick-prev:before, .slick-next:before {
    opacity: 1;
    color: #3d3d3d;
    font-size: 25px;
}

.slick-slide {
	outline: none !important;
	margin: 0 0 0 0 !important;
}

.slick-slide div img {
	box-shadow: none !important;
	outline: none !important;
	max-width: 190px;
	margin: 0 auto;
}

.slick-slide div {
	outline: none !important;
}

.partner-school-slider {
    margin: 0 15px !important;
}
</style>

<?php $logos = get_field('logos'); ?>

<?php if( $logos ): ?>

	<ul class="partner-school-slider">

		<?php foreach($logos as $logo) : ?>
		    <li>
		    	<div>
		    		<img src="<?php echo $logo['image']; ?>" alt="<?php echo $logo['alt']; ?>">
		    	</div>
		    </li>
		<?php endforeach; ?>

		<?php foreach($logos as $logo) : ?>
		    <li>
		    	<div>
		    		<img src="<?php echo $logo['image']; ?>" alt="<?php echo $logo['alt']; ?>">
		    	</div>
		    </li>
		<?php endforeach; ?>

	</ul>

	<script src="<?php echo bloginfo('stylesheet_directory'); ?>/js/slick.min.js"></script>

	<script>
	(function($) {

		// slider init
		$('.partner-school-slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 4000,
			responsive: [
			{
			breakpoint: 990,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
			breakpoint: 766,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		  ]
		});

	})( jQuery );
	</script>

<?php endif; ?>