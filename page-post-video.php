<?php
/**
 * The Header for your theme.
 *
 * Displays all of the <head> section and everything up until <div id="main">
 *
 * @package WordPress
 * @subpackage Boss
 * @since Boss 1.0.0
 */
?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="msapplication-tap-highlight" content="no"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<!-- BuddyPress and bbPress Stylesheets are called in wp_head, if plugins are activated -->
        <?php wp_head(); ?>
        <style>
            html, body {
                background-color: white !important;
                margin: 0px !important;
                overflow: hidden;
            }
            
            body {
                padding: 16px;
            }

            #ifc-launcher-app {
                display: none;
            }
        </style>
	</head>

	<body <?php body_class(); ?> data-logo="<?php echo $logo; ?>" data-inputs="<?php echo $inputs; ?>" data-rtl="<?php echo ($rtl) ? 'true' : 'false'; ?>" data-header="<?php echo $header_style; ?>">
        <?php the_content(); ?>
        <?php
        /**
         * The template for displaying the footer.
         *
         * Contains footer content and the closing of the
         * #main and #page div elements.
         *
         * @package WordPress
         * @subpackage Boss
         * @since Boss 1.0.0
         */
        ?>
        <?php wp_footer(); ?>
    </body>
</html>