var immerseLmsAjax = new ImmerseLms_Ajax(jQuery);

jQuery(function($) {
    var QUIZ_ATTEMPTS = window.QUIZ_ATTEMPTS || 0;
    var MAX_QUIZ_ATTEMPTS = window.MAX_QUIZ_ATTEMPTS || null;
	
    $(document).on("click", "[data-ajax-click]", function(e) {
        var $this = $(this);
        var functionName = $this.data("ajax-click");
        var args = [];

        getParamNames(immerseLmsAjax[functionName]).forEach(function(name) {
            var val = $this.data("ajax-arg-" + name.replace(/_/g, "-"));

            if (typeof val == 'string' && val.indexOf("js:") == 0) {
                val = eval(val.substring(3));
            }

            args.push(val);
        });

        var startAction = $this.data("ajax-start");

        if (startAction) {
            eval(startAction);
        }

        immerseLmsAjax[functionName]
            .apply(immerseLmsAjax, args)
            .then(function(data) {
                var successAction = $this.data("ajax-success");

                if (successAction) {
                    eval(successAction);
                }
            })
            .catch(function(e) {
                alert("Error: " + e.message);
                console.log(e);
            })
            .then(() => {
                $this.prop("disabled", false);
                var doneAction = $this.data("ajax-done");

                if (doneAction) {
                    eval(doneAction);
                }
            });
        
        $this.prop("disabled", true);
    });
    
    var startRestartButton = $(".wpProQuiz_button[name=restartQuiz], .wpProQuiz_button[name=startQuiz]");
    startRestartButton.before("<div style='margin-bottom: 26px; text-align: center; font-size: 16px; font-weight: bold;'>Contact your tutor to get help with this quiz.</div>");
    
    if (MAX_QUIZ_ATTEMPTS) {
        var started = false;
        var attemptsText = $("<div />").addClass("attempts-text");

        setInterval(function() {
            $(".wpProQuiz_quiz .wpProQuiz_question_page:hidden").show();

            if (started) {
                if (QUIZ_ATTEMPTS >= MAX_QUIZ_ATTEMPTS) {
                    startRestartButton.remove();
                }
            }

            $(".attempts-text").text("Attempt " + QUIZ_ATTEMPTS + " of " + MAX_QUIZ_ATTEMPTS);
        }, 50);

        startRestartButton.click(function() {
            QUIZ_ATTEMPTS++;
            started = true;
        });

        startRestartButton.after(attemptsText);
        
        if (QUIZ_ATTEMPTS >= MAX_QUIZ_ATTEMPTS) {
            startRestartButton.remove();
        }
    }

    $(".rm-tab-option").click(function(e) {
        var container = $(this).parent().parent();

        container.find(".rm-tab").hide();
        container.find("#" + $(this).attr("href").substring(1)).show();

        $(this).parent().find(".rm-tab-option").removeClass("selected");
        $(this).addClass("selected");
        
        e.preventDefault();
        e.stopPropagation();
    });

    $(".lesson-action[data-action=expand]").click(function() {
        $(this).find("i").toggleClass("fa-chevron-down").toggleClass("fa-chevron-up");
        $(this).parent().find(".lesson-children").slideToggle();
    });

    $(".consolidated-quiz-report-form").each(function() {
        var form = $(this);
        var button = form.find("button");
        var progressBarContainer = form.find(".progress-bar-container");
        var progressBar = form.find(".progress-bar");
        var progressText = form.find(".progress-text");

        progressBarContainer.hide();
        
        button.click(function() {
            progressBarContainer.show();
            var students = ALL_STUDENTS;
            var allData = [];
            
            function next(i) {
                progressBar.css("width", (i / students.length) * 100 + "%");

                if (i < students.length) {
                    progressText.text("Downloading student " + (i + 1) + "/" + students.length + "...");
                    var studentId = students[i];

                    immerseLmsAjax.exportQuizData(studentId).then((data) => {
                        if (data) {
                            allData = allData.concat(data);
                        }
                        
                        next(i + 1);
                    });
                } else {
                    progressBar.css("width", "100%");
                    progressText.text("Done.");
                    downloadCsv(['user_id', 'name', 'email', 'quiz_id', 'quiz_title', 'score', 'total', 'date', 'percentage', 'time_spent', 'passed', 'course_id', 'course_title'], allData, "Consolidated");
                }
            }

            next(0);
        });
    });

    $(document).ajaxSend(function(e, xhr, settings) {
        if (typeof(settings.data) == "string") {
            var action = /^action=(\w+?)\&/g.exec(settings.data);
            action = action ? action[1] : null;

            if (action == "ld_adv_quiz_pro_ajax") {
                xhr.fail(function(xhr, status, error) {
                    alert("Please check your dashboard if the quiz was saved." + (error ? (" (Error: " + error + ")") : "") +
                    " (Code: " + action + "-" + xhr.status + "-" + Date.now() + ")");
                });
            }

            if (action == "wp_pro_quiz_completed_quiz") {
                var pairs = settings.data.split("&");
                var data = {};

                pairs.forEach(function(pair) {
                    pair = pair.split("=");
                    data[pair[0]] = decodeURIComponent(pair[1] || "");
                });

                var LAST_QUIZ_ATTEMPTS = -1;
                var next = null;

                immerseLmsAjax.countStatisticRefs(data.quizId)
                .then(function(sri) {
                    LAST_QUIZ_ATTEMPTS = sri;

                    if (next) {
                        next();
                    }
                });

                xhr.fail(function(xhr, status, error) {
                    var totalFailure = function() {
                        alert("Please check your dashboard if the quiz was saved." + (error ? (" (Error: " + error + ")") : "") +
                        " (Code: " + action + "-" + xhr.status + "-" + Date.now() + ")");

                        // Redirect to My Courses page.
                        window.location.href = "/my-courses/";
                    }
                    // Try another XHR.
                    immerseLmsAjax.countStatisticRefs(data.quizId)
                    .then(function(sri) {
                        next = function() {
                            if (sri == LAST_QUIZ_ATTEMPTS) {
                                // Uh oh, quiz wasn't saved.
                                alert("Unfortunately, your quiz wasn't saved and must be retaken.");
                                window.location.reload();
                            } else {
                                LAST_QUIZ_ATTEMPTS = sri;

                                immerseLmsAjax.getQuizAttempt(data.quizId)
                                .then(function(quiz) {                                    
                                    // Quiz was saved.
                                    alert("Your quiz was saved, and your score is: " + quiz.score + "/" + quiz.total);
                                    window.location.href = "/my-courses/";
                                })
                                .catch(function(e) {
                                    alert("Your quiz was saved, but unfortunately your score could not be retrieved.");
                                    window.location.href = "/my-courses/";
                                });
                            }
                        };

                        if (LAST_QUIZ_ATTEMPTS > -1) {
                            next();
                        }
                    })
                    .catch(function(e) {
                        totalFailure();
                    });
                });
            }

            // Show loading screen for masquerade.
            if (action == "immerse_lms_ajax_public_masquerade") {
                $("<div />")
                    .hide()
                    .css({
                        position: 'fixed',
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0,
                        backgroundColor: 'rgba(0, 0, 0, 0.25)',
                        paddingTop: '20%',
                        zIndex: 10000,
                        textAlign: 'center',
                        color: 'white'
                    })
                    .append(
                        $("<div />")
                        .addClass("loader-content")
                        .css({
                            marginLeft: 'auto',
                            marginRight: 'auto',
                            width: 32,
                            height: 32
                        })
                    )
                    .append(
                        "Switching user..."
                    )
                    .appendTo("body")
                    .fadeIn();

                xhr.fail(function(xhr, status, error) {
                    alert("Masquerade failed. Error: " + (error ? (" (Error: " + error + ")") : "") +
                    " (Code: " + action + "-" + xhr.status + "-" + Date.now() + ")");
                });
            }
        }
    });

    function injectMarkup() {
        // For our add photo, progress and preview area we rely
        // on #what-new-content
        setTimeout(function () {
            var $whats_new_content = $( '#whats-new-options' );
            var additional = $whats_new_content.find("#whats-new-additional");
            var newAdditional = $(`
            <div id="whats-new-additional">
                <div id="buddyboss-media-add-photo">
                    <button type="button" class="buddyboss-activity-media-add-photo-button rm-upload-video" style=""></button>
                </div>
            </div>
            `);
            additional.parent().prepend(newAdditional);
            var textbox = additional.parent().parent().find("textarea");
            var submit = additional.parent().parent().find("#aw-whats-new-submit");

            newAdditional.find("button").click(function() {
                var modal = iframeModal(800, 720, "/post-video/", "video-upload");

                window.videoUploadCallback = function(text, url) {
                    modal.fadeOut(400, function() {
                        modal.remove();
                    });
                    textbox.val(text + "\n\n" + url).focus().change();
                    submit.click();
                };
            });
        }, 2000 );
    }

    injectMarkup();

    $("[id=buddyboss-media-add-photo]").each(function() {
        var addPhoto = $(this);
        var additionals = addPhoto.parent();
        additionals.append(addPhoto.clone());
    });
});

function createCsvText(cols, rows) {
    return cols.join(',') + '\n' +
        rows.map(function(row) {
            return cols.map(function(column) {
                return row[column] ? ('"' + row[column].toString().replace(/"/g, '\\"') + '"') : '';
            }).join(',');
        }).join('\n');
}

function downloadCsv(cols, rows, name) {
    var text = createCsvText(cols, rows);

    var link = document.createElement('a');
    link.setAttribute('href', 'data:text/csv;charset=utf8,' + text);
    link.setAttribute('download', 'export_' + name + '_' + Math.floor((new Date()).getTime() / 1000) + '.csv');
    link.click();
}

function getHierarchyData(id) {
    var lessons = [];

    $("[data-course-id=" + id + "]").find("[data-lesson-id]").each(function() {
        if ($(this).find("input[type=checkbox]").prop("checked")) {
            var lesson = {
                id: $(this).data("lesson-id"),
                children: JSON.parse($(this).find("input[type=hidden]").val())
            };

            lessons.push(lesson);
        }
    });

    return lessons;
}

function printNote(noteId) {
    window.open("/print-note/?note_id=" + noteId, "Note Printer", "width=800,height=600");
}

function iframeModal(width, height, url, cl) {
    var mainContainer = jQuery("<div />")
        .css({
            position: "fixed",
            width: "100%",
            height: "100%",
            backgroundColor: "rgba(0, 0, 0, 0.2)",
            zIndex: 1000
        })
        .addClass(cl)
        .hide()
        .appendTo(document.body)
        .fadeIn();
    
    var modalContainer = jQuery("<div />")
    .css({
        width: width,
        height: height,
        backgroundColor: "white",
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: 25
    });

    mainContainer.append(modalContainer);

    var iframe = jQuery("<iframe />")
    .css({
        width: "100%",
        height: "100%",
        overflow: "hidden"
    })
    .attr("src", url);

    modalContainer.append(iframe);

    return mainContainer;
}